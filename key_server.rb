require 'sinatra/base'
require 'securerandom'
require 'eventmachine'

class KeyManager

  GENERATE_KEYS_BATCH_NUMBER = 1  # At a time generate how many available keys
  MAX_KEYS_AVAILABLE = 1000  # Hard limit on number of available keys.
  KEEP_ALIVE_TIMEOUT = 300  # in seconds

  def initialize
    @keysLock = Mutex.new
    @keys = []
    @blocked_keys = {}
  end

  def generate_new_keys
    return false if @keys.length >= MAX_KEYS_AVAILABLE
    randomKeys = []
    GENERATE_KEYS_BATCH_NUMBER.times {
      randomKeys << SecureRandom.uuid
    }
    @keysLock.synchronize {
      @keys.push(*randomKeys)
    }
  end

  def keep_alive_key_timer(key)
    EventMachine::Timer.new(KEEP_ALIVE_TIMEOUT) do
      @keysLock.synchronize {
        @blocked_keys.delete(key)
      }
    end
  end

  def available
    key = nil
    @keysLock.synchronize {
      key = @keys.shift
      if !key.nil?
        @blocked_keys[key] = {:timer => keep_alive_key_timer(key)}
      end
    }
    key
  end

  def unblock(key)
    return false if @blocked_keys[key].nil?
    @keysLock.synchronize {
      key_present = @blocked_keys.delete(key)
      @keys.push(key) if !key_present.nil?
    }
    true
  end

  def delete_key(key)
    return false if @blocked_keys[key].nil?
    @keysLock.synchronize {
      key_present = @blocked_keys.delete(key)
      return false if !key_present
    }
    true
  end

  def keep_alive(key)
    return false if @blocked_keys[key].nil?
    @keysLock.synchronize {
      if !@blocked_keys[key].nil?
        old_timer = @blocked_keys[key][:timer]
        old_timer.cancel
        @blocked_keys[key][:timer] = keep_alive_key_timer(key)
        return true
      end
    }
    false
  end

  def to_s
    "AvailableKeys:#{@keys}, BlockedKeys:#{@blocked_keys}"
  end
end

class App < Sinatra::Base

  def initialize
    super
    @key_manger = KeyManager.new
    EM.next_tick {}
  end

  post '/generate_keys' do
    if @key_manger.generate_new_keys
      status 200
    else
      status 500
      body 'Server reached the limit of generating api keys'
    end
  end

  get '/available_key' do
    key = @key_manger.available
    if key
      status 200
      body "#{key}"
    else
      status 404
    end
  end

  put '/unblock_key/:key' do
    if !params['key'].nil? && params['key'] != ''
      if @key_manger.unblock(params['key'])
        status 200
      else
        status 404
      end
    else
      status 404
    end
  end

  delete '/delete_key/:key' do
    if !params['key'].nil? && params['key'] != ''
      if @key_manger.delete_key(params['key'])
        status 200
      else
        status 404
      end
    else
      status 404
    end
  end

  get '/keep_alive/:key' do
    if !params['key'].nil? && params['key'] != ''
      if @key_manger.keep_alive(params['key'])
      else
        status 404
      end
    else
      status 404
    end
  end

  get '/keysinfo' do
    body "#{@key_manger.to_s}"
  end
end